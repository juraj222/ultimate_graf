#include <iostream>
#include <string>
#include <list>
#include <set>
#include <map>
#include <vector>
#include <fstream>
#include <algorithm>
#include <queue>
#include <stack>
#include <limits>

using namespace std;

class Vrchol
{
private:
    string name;
    list<Vrchol*> susedia;
public:
    Vrchol(string name)
    {
        this->name = name;
    }

    string getName()
    {
        return this->name;
    }

    list<Vrchol*> *getList()
    {
        return &susedia;
    }



};
class Compare {
public:
	bool operator()(pair<Vrchol*,int>& t1, pair<Vrchol*,int>& t2)
	{
		if (t1.second > t2.second) return true;
		return false;
	}
};

class MyGraph
{
private:
    list<Vrchol*> graph;


    void addEdge(string name_1, string name_2)
    {
        Vrchol *prm = findVrcholByName(name_1);
        prm->getList()->push_back(new Vrchol(name_2));
    }

    void addVrchol(string name)
    {
        //kotrola ci taky vrchol neexistuje pokial hej uz sa neprida
        for(list<Vrchol*>::iterator it = graph.begin(); it != graph.end(); it++){
            if((name.compare((*it)->getName())) == 0)
                return;
        }
        graph.push_back(new Vrchol(name));
    }


public:

    Vrchol *findVrcholByName(string name)
    {
        Vrchol *prm = NULL;
        for(list<Vrchol*>::iterator it = graph.begin(); it != graph.end(); it++)
        {
            if(name.compare((*it)->getName()) == 0)
            {
                prm = (*it);
            }
        }
        return prm;
    }

    void printGraphVrcholy()
    {
        for(list<Vrchol*>::iterator it = graph.begin(); it != graph.end(); it++)
        {
            cout << (*it)->getName()<<endl;
        }
    }

    void loadFromFile(string fileName){
        ifstream file(fileName.c_str());
        string vrchol1;
        string vrchol2;
        while(!file.eof()){
            file >> vrchol1;
            file >> vrchol2;
            addVrchol(vrchol1);
            addVrchol(vrchol2);
            addEdge(vrchol1, vrchol2);
        }
    }

    void printSusedou()
    {
        for(list<Vrchol*>::iterator it = graph.begin(); it != graph.end(); it++)
        {
            if(!((*it)->getList()->empty()))
            {
                for(list<Vrchol*>::iterator it_2 = (*it)->getList()->begin(); it_2 != (*it)->getList()->end(); it_2++)
                {
                    cout << (*it)->getName() << " sused " << (*it_2)->getName() << endl;
                }
            }
        }
    }

    list<Vrchol*> BFS(Vrchol* start)
    {
        list<Vrchol*> result;
        list <Vrchol*> susedia;
        queue<Vrchol*> fifo;
        fifo.push(start);
        while (!fifo.empty())
        {
            result.push_back(fifo.front());
            fifo.pop();
            susedia = vratSusedov(result.back()->getName());
            for(list<Vrchol*>::iterator it = susedia.begin(); it != susedia.end(); it++)
            {

                if(!findVrcholByName((*it)->getName(),result))
                {
                    fifo.push(*it);
                }
            }
        }

        for(list<Vrchol*>::iterator it = result.begin(); it != result.end(); it++)
        {
            cout << (*it)->getName() << endl;
        }

        return result;
    }


    map<Vrchol*,int> Dijkstra(Vrchol * start){
		map<Vrchol*,int> dlzkaCesty;
		Vrchol * vrchol =start;
		list<Vrchol*> susedia;
		list<Vrchol*> unvisited(graph);


		priority_queue<pair<Vrchol*,int>,vector<pair<Vrchol*,int> >, Compare> fronta;

		dlzkaCesty[vrchol] = 0;
		list<Vrchol*>::iterator it=graph.begin();
		it++;
		for(it; it != graph.end();it++)
		{
			dlzkaCesty.insert( pair<Vrchol*,int>((*it),numeric_limits<int>::max()));
		}

		fronta.push(make_pair(vrchol,dlzkaCesty[vrchol]));

		while(!fronta.empty()){
			vrchol=fronta.top().first;
			susedia = vratSusedov(vrchol->getName());
			fronta.pop();
			for(list<Vrchol*>::iterator it = susedia.begin(); it!=susedia.end();it++){
				if(findVrcholByName((*it)->getName(),unvisited)){
					Vrchol * v = findVrcholByName((*it)->getName());
					if(1 + dlzkaCesty[vrchol] < dlzkaCesty[findVrcholByName((*it)->getName())]){
						dlzkaCesty[findVrcholByName((*it)->getName())] = 1+ dlzkaCesty[vrchol];
					}
					fronta.push(make_pair(findVrcholByName((*it)->getName()),dlzkaCesty[findVrcholByName((*it)->getName())]));
				}
			}
			unvisited.remove(vrchol);
		}
		return dlzkaCesty;
	}

	int najkratsiaCesta(string V1, string V2){

		int vzdialenost =0;
		map<Vrchol*,int> m1 = Dijkstra(findVrcholByName(V1));
		for(map<Vrchol*,int>::iterator it = m1.begin(); it!=m1.end(); it++){
			if((*it).first->getName() == V2){
				vzdialenost+=(*it).second;
				break;
			}
		}
		return vzdialenost;
	}


    list<Vrchol*> vratSusedov(string meno)
    {
        list<Vrchol*> vvv;
        Vrchol* v = findVrcholByName(meno);
        for(list<Vrchol*>::iterator it = v->getList()->begin(); it != v->getList()->end(); it++)
        {
            vvv.push_back((*it));
        }
        return vvv;
    }

    bool findVrcholByName(string s,list<Vrchol*> v){
		for(list<Vrchol*>::iterator it = v.begin(); it != v.end(); it++) {
			if( (*it)->getName() == s) {
				return true;
			}
		}
		return false;
	}

    bool** ulozDoMatice()
    {
        bool var_matica = new bool*[graph.size()];
        for(int i = 0; i < graph.size(); i++)
        {
            var_matica[i] = new bool[graph.size()];
            for(int j = 0; j < graph.size(); j++)
            {
                var_matica[i][j] = false;
            }
        }
        //graf musi bzt reprezentovany cisli, teray by bol problem s prevodom

    }

};


int main()
{
    cout << "Hello world!" << endl;
    MyGraph mg;
    mg.loadFromFile("zdroj.txt");
    mg.printGraphVrcholy();
    mg.printSusedou();
    mg.BFS(mg.findVrcholByName("C"));
    cout<< "najkratsia cesta mezi A F"<< mg.najkratsiaCesta("A", "F") << " pokial neni haze blbosti";

    return 0;
}
